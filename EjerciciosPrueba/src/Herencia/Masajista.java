
package Herencia;

public class Masajista extends SeleccionFutbol{
    private int anosExperiencia;
    private String titulacion;

    public Masajista(int anosExperiencia, String titulacion, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.anosExperiencia = anosExperiencia;
        this.titulacion = titulacion;
    }
    
    public String getAtributos() {
        return "ID: " + id
                + "\nEdad: " + edad
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                +"\nTitulacion: "+titulacion
                +"\nAños de Experiencia: "+anosExperiencia;
    }
    
      public void Viajar() {
        System.out.println("Viajando a Europa");
    }
    
     public void Concentrarse() {
        System.out.println("No concenrado");

    }
     
     public void darMasaje(){
         System.out.println("Masajeando futbolista");
     }
     
     
}
    
