
package Herencia;

public class Entrenador extends SeleccionFutbol{
    private String idFederacion;

    public Entrenador(String idFederacion, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.idFederacion = idFederacion;
    }
    
     public String getAtributos() {
        return "ID: " + id
                + "\nEdad: " + edad
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                +"\nID Federación: "+idFederacion;
       
    }
      public void Viajar() {
        System.out.println("No viaja");
    }
    
     public void Concentrarse() {
        System.out.println("Concentrado");

    }
     
     public void dirigirPartido(){
         System.out.println("Dirigiendo partido");
     }
     
     public void dirigirEntrenamiento(){
         System.out.println("No dirige");
     }
}
