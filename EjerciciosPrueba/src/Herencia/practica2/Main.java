package Herencia.practica2;

import Herencia.SeleccionFutbol;

public class Main {

    public static void main(String[] args) {

        Productos productos = new Productos("12/12/2020", 848545, "12/12/2019", "Panamá");

        Frescos frescos = new Frescos("15/05/2020", 5915, "12/01/2018", "Italia");

        Refrigerados refri = new Refrigerados(1500, 15, "07/02/2021", 411, "02/03/2020", "España");

        Congelados conge = new Congelados(50, "15/02/2019", 1541, "10/10/2018", "Australia");

        // System.out.println(productos.getAtributos());
    
        System.out.println("☼ PRODUCOS CONGELADOS ☼ ");
        System.out.println(conge.getAtributos());
        System.out.println("** POR AIRE **");
        conge.aire();
        System.out.println("** POR AGUA **");
        conge.agua();
        System.out.println("** POR NITROGENO **");
        conge.nitrogeno();

        System.out.println("");

        System.out.println("☼ PRODUCOS REFRIGERADOS ☼ ");
        System.out.println(refri.getAtributos());

        System.out.println("");

        System.out.println("☼ PRODUCOS FRESCOS ☼ ");
        System.out.println(frescos.getAtributos());

    }
}
