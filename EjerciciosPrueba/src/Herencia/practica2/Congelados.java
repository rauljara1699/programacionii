
package Herencia.practica2;

public class Congelados extends Productos{
    private int temperatura;

    public Congelados(int temperatura, String fechaCaducidad, int lote, String fechaEnvadado, String pais) {
        super(fechaCaducidad, lote, fechaEnvadado, pais);
        this.temperatura = temperatura;
    }

    

    public String getAtributos() {
        return "Fecha vencimiento: " + fechaCaducidad
                + "\nNúmero de lote: " + lote
                + "\nFecha Envasado: " + fechaEnvadado
                + "\nPaís: " + pais
                +"\nTemperatura recomendada: "+temperatura;
                
    }
    
    public void aire(){
        System.out.println("20% Nitrogeno");
        System.out.println("50% Oxigeno");
        System.out.println("5% Dioxido de carbono");
        System.out.println("25% Vapor de aire");
    }
    
    public void agua(){
        int sal = 20;
        int agua= 50;
        int salinidad= sal*agua;
        System.out.println("Salinidad: "+salinidad);
    }
    
    public void nitrogeno(){
        System.out.println("Metodo empleado: Rapido");
        System.out.println("Tiempo: 2500 segundos");
    }
    
}
