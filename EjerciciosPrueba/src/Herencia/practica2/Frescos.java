
package Herencia.practica2;

public class Frescos extends Productos{

    public Frescos(String fechaCaducidad, int lote, String fechaEnvadado, String pais) {
        super(fechaCaducidad, lote, fechaEnvadado, pais);
    }

    
    
     public String getAtributos() {
        return "Fecha vencimiento: " + fechaCaducidad
                + "\nNúmero de lote: " + lote
                + "\nFecha Envasado: " + fechaEnvadado
                + "\nPaís: " + pais;
                
    }
    
}
