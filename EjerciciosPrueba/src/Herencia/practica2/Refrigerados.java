package Herencia.practica2;

public class Refrigerados extends Productos {

    private int codigoOrganismo, temperatura;

    public Refrigerados(int codigoOrganismo, int temperatura, String fechaCaducidad, int lote, String fechaEnvadado, String pais) {
        super(fechaCaducidad, lote, fechaEnvadado, pais);
        this.codigoOrganismo = codigoOrganismo;
        this.temperatura = temperatura;
    }

    

    public String getAtributos() {
        return "Fecha vencimiento: " + fechaCaducidad
                + "\nNúmero de lote: " + lote
                + "\nFecha Envasado: " + fechaEnvadado
                + "\nPaís: " + pais
                +"\nCodigo del Organismo: " + codigoOrganismo
                + "\nTemperatura recomendada: " + temperatura;
    }
    
    

}
