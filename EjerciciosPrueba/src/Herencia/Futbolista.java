/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author raulj
 */
public class Futbolista extends SeleccionFutbol{
    
    private int dorsal;
    private String demarcacion;

    public Futbolista(int dorsal, String demarcacion, int id, int edad, String nombre, String apellidos) {
        super(id, edad, nombre, apellidos);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    
     public String getAtributos() {
        return "ID: " + id
                + "\nEdad: " + edad
                + "\nNombre: " + nombre
                + "\nApellidos: " + apellidos
                +"\nDorsal: "+dorsal
                +"\nDemarcación: "+demarcacion;

    }
    
      public void Viajar() {
        System.out.println("No Viaja");
    }
    
     public void Concentrarse() {
        System.out.println("No concentrado");

    }
     
     public void JugarPartido(){
         System.out.println("Jugando partido");
     }
     
     public void entrenar(){
         System.out.println("Entrenando");
     }
     
     
    
    
    
}
